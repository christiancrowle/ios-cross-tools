## Cross compiling from Linux x86_64 to Darwin aarch64 using Podman (or Docker)

This repository contains a `Dockerfile` that builds an LLVM toolchain for `aarch64-apple-darwin` (hosted on
 Linux). This is not a very well documented process, and it's even more of a pain in the ass using a container. But here we are. ¯\\\_(ツ)\_/¯

 Based on the documentation [here](https://docs.godotengine.org/en/stable/development/compiling/cross-compiling_for_ios_on_linux.html), and [here](https://github.com/ProcursusTeam/Procursus/wiki/Building-on-Linux). Thanks guys!

 ## Usage:

You'll need a copy of `Xcode_11.3.1.xip` (`sha256 9a92379b90734a9068832f858d594d3c3a30a7ddc3bdb6da49c738aed9ad34b5`) 
in the root of the repository right next to the Dockerfile, which means you'll need an Apple developer account. You
 can easily find all of the Xcode releases at [xcodereleases.com](https://xcodereleases.com). Other versions may work,
 but for now it's hardcoded into the Dockerfile. I should fix this.

After that, you can run `make run` to build the container and enter it. The Makefile uses podman (explained below), 
but Docker will work as well (but it might be slower).

The container contains a full LLVM toolchain, prefixed with `aarch64-apple-darwin`. The signing tool `ldid` is 
included as well, and so is a very simple sample `entitlements.plist` to sign basic binaries, which you can do like 
this:

`ldid -S/opt/iphone-toolchain/entitlements.plist <your executable>`

This is required for every executable you want to run. If you skip this step, you'll get `Killed: 9` every time you 
try to run an executable, because the iOS sandboxing stuff will SIGKILL anything that doesn't have valid entitlements.

## Info/How it Works:

The toolchain is produced by extracting the iPhoneOS SDK and header files from the Xcode image you provide. Then we 
use [a port of Apple's cctools to Linux](https://github.com/tpoechtrager/cctools-port) to combine that SDK with the 
system `clang` and produce (hopefully) a working toolchain. See the comments in the Dockerfile for more info. 
(Hopefully it's easy to follow)

I'm using podman in the Makefile instead of Docker. That's because Docker (without BuildKit) has to send the entire source directory
 to the daemon, which is really slow when you've got a gigantic Xcode image hanging out in there. Using podman (which doesn't have 
 a daemon) sidesteps this issue completely.

## Thanks

Thanks to [Thomas Pöchtrager](https://github.com/tpoechtrager) for maintaining the [cctools port](https://github.com/tpoechtrager/cctools-port),

[Niklas Rosenstein](https://github.com/nrosenstein-stuff/pbzx) for maintaining [pbzx](https://github.com/nrosenstein-stuff/pbzx),

and Apple for not requiring me to have a Mac to download Xcode. :P

**By the way, the Dockerfile is in the Public Domain. Modify it or whatever, it's a Dockerfile, I don't care**