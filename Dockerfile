FROM fedora:35

COPY Xcode_11.3.1.xip /opt/Xcode_11.3.1.xip

RUN dnf install -y clang fuse-devel libicu-devel openssl-devel \
        xz-devel git cmake xar xar-devel cpio xz findutils sed wget unzip nano file

RUN mkdir -p /opt/iphone-toolchain /opt/iphone-toolchain/sdks

# First we've got to compile and install pbzx. this will help us extract the Xcode xip later
RUN git clone https://github.com/nrosenstein-stuff/pbzx && \
    cd pbzx && \
    gcc pbzx.c -o pbzx -llzma -lxar && \
    install -m755 pbzx /usr/bin/

# Now we extract the Xcode xip, copy the iPhoneOS sdk out of it, and take the C++ header files from
#  the toolchain.
RUN mkdir -p /tmp/xcode && \
    xar -xf /opt/Xcode_11.3.1.xip -C /tmp/xcode && \
    cd /tmp/xcode && \
    pbzx -n Content | cpio -i && \
    rm Content && \
    mkdir -p /opt/iphone-toolchain/sdks/iPhoneOS13.2.sdk && \
    cp -r /tmp/xcode/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS.sdk/* /opt/iphone-toolchain/sdks/iPhoneOS13.2.sdk && \
    cp -r /tmp/xcode/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include/c++/* /opt/iphone-toolchain/sdks/iPhoneOS13.2.sdk/usr/include/c++ && \
    rm -rf /tmp/xcode && \
    rm /opt/Xcode_11.3.1.xip

# Finally, we tar up our rebuilt iPhoneOS SDK, clone the port of the Apple cctools, and (hopefully) 
#  build a working aarch64-apple-darwin LLVM toolchain (we change the triple to something that makes 
#  more sense)
RUN cd /opt/iphone-toolchain/sdks/ && \
    tar -czf iPhoneOS13.2.sdk.tar.gz iPhoneOS13.2.sdk && \
    git clone https://github.com/tpoechtrager/cctools-port /opt/iphone-toolchain/cctools-src && \
    cd /opt/iphone-toolchain/cctools-src/usage_examples/ios_toolchain && \
    sed -i "s/arm-apple-darwin11/aarch64-apple-darwin/" build.sh && \
    ./build.sh /opt/iphone-toolchain/sdks/iPhoneOS13.2.sdk.tar.gz arm64 && \
    cp -rv /opt/iphone-toolchain/cctools-src/usage_examples/ios_toolchain/target/* /usr/local/

COPY entitlements.plist /opt/iphone-toolchain/entitlements.plist