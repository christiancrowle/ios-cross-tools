build:
	podman build --force-rm -t picsofbread/ios-cross .
	touch build

run: build
	podman run -it localhost/picsofbread/ios-cross

build-and-run: clean run

clean:
	rm -f build

total-clean:
	@printf "\033[1;36mDon't worry, this is allowed to fail!\033[0m\n"
	-podman container prune -f
	-podman image prune -a -f
	-podman system prune -a -f
	-buildah rm -a
